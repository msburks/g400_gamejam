﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController gc;

    public int deaths;
    public int time;
    public GameObject player;
    public bool gameOver;


    //all the abailities settings
    public bool cheetahFeet;
    public bool frogFeet;

    public bool clawArms;
    public bool lizardArms;

    public bool wingsExtra;
    public bool fishExtra;




    private void Awake()
    {
        if (gc == null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
            return;
        }

   

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    
}
