﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    int n;
    public GameObject clawImage;
    public GameObject lizardImage;
    public GameObject cheetahImage;
    public GameObject frogImage;
    public GameObject fishImage;
    public GameObject wingsImage;




    // Start is called before the first frame update
    void Start()
    {
        clawImage.SetActive(false);
        lizardImage.SetActive(false);
        cheetahImage.SetActive(false);
        frogImage.SetActive(false);
        fishImage.SetActive(false);
        wingsImage.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonPress()
    {
        n++;
        Debug.Log("Button clicked " + n + " times.");
    }

    //button to toggle claw image in character creation screen
    public void ClawButton()
    {
        Debug.Log("Button has been hit!");
        clawImage.SetActive(true);
        lizardImage.SetActive(false);

        //set booleans for charater selection in GC

        GameController.gc.clawArms = true;
        GameController.gc.lizardArms = false;

    }

    //button to toggle lizard image in character creation screen
    public void LizardButton()
    {
        Debug.Log("Button has been hit!");
        lizardImage.SetActive(true);
        clawImage.SetActive(false);

        //set booleans for charater selection in GC
        GameController.gc.clawArms = false;
        GameController.gc.lizardArms = true;

    }

    //button to toggle cheetah image in character creation screen
    public void CheetahButton()
    {
        Debug.Log("Button has been hit!");
        cheetahImage.SetActive(true);
        frogImage.SetActive(false);

        //set booleans for charater selection in GC
        GameController.gc.cheetahFeet = true;
        GameController.gc.frogFeet = false;

    }

    //button to toggle frog image in character creation screen
    public void FrogButton()
    {
        Debug.Log("Button has been hit!");
        frogImage.SetActive(true);
        cheetahImage.SetActive(false);

        //set booleans for charater selection in GC
        GameController.gc.cheetahFeet = false;
        GameController.gc.frogFeet = true;

    }

    //button to toggle fish image in character creation screen
    public void FishButton()
    {
        Debug.Log("Button has been hit!");
        fishImage.SetActive(true);
        wingsImage.SetActive(false);

        //set booleans for charater selection in GC
        GameController.gc.fishExtra = true;
        GameController.gc.wingsExtra = false;

    }

    //button to toggle wings image in character creation screen
    public void WingsButton()
    {
        Debug.Log("Button has been hit!");
        wingsImage.SetActive(true);
        fishImage.SetActive(false);

        //set booleans for charater selection in GC
        GameController.gc.fishExtra = false;
        GameController.gc.wingsExtra = true;

    }

    public void StartButton()
    {
        SceneManager.LoadScene("ButtonTestScene");
    }
}
